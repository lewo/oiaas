Scripts here are used to build images where the configuration is
initially applied. This is how you can easily test the configuration.

# Clone the nixpkgs fork

```
git clone https://github.com/nlewo/nixpkgs.git
git checkout origin/oiaas
```

# Deploy the configuration with with nixops (libvirt)

First install [Nixops](https://nixos.org/nixops/).

You need to specify the PATH to the previously cloned nixpkgs repository
```
nixops create -I nixpkgs=YOUR-NIXPKGS-PATH nixops.nix
nixops deploy
```

You can then display journald logs:
```
export VM_IP=$(nixops info --plain | awk -F " " '{ print $4 }')
curl http://${VM_IP}:19531/entries
```

# Notes

There are some settings that are dependent from the machine where the
configuration is deployed. This is for instance the case grub and
hardware configuration.

To create some files that are specific to a machine, we enable a
boostrap systemd service called `nixos-bootstrap`. This service
currently installs the grub configuration and generates the hardware
configuration before running any `nixos-rebuild`.
